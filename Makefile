setup:
	python3 -m pip install pipenv
	@pipenv --rm && echo "Removed virtualenv." || echo "No virtualenv to remove."
	pipenv install
	pipenv install -d --pre
	pipenv run ./manage.py collectstatic --no-input
	$(MAKE) setup -C build_js_css

js:
	$(MAKE) build -C build_js_css
	$(MAKE) install -C build_js_css

check: 
	pipenv run black --check .
	pipenv run isort --check .
	pipenv run bandit -r . -ll -x ./build_js_css
	pipenv run ./manage.py check
	$(MAKE) check -C build_js_css
	

format:
	@pipenv run black .
	@pipenv run isort .
test:
	@pipenv run pytest -v --cov=.	

