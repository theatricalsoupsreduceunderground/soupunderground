import csv
import json

if __name__ == "__main__":
    answerlist = list()
    with open("misc/okc_questions.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                question = row[0]
                options = list()

                for element in [row[1], row[2], row[3], row[4]]:
                    if element != "":
                        options.append(element)

                answer = {
                    "model": "core.question",
                    "fields": {
                        "question_text": question,
                        "pub_date": "2020-12-28T13:30:26Z",
                        "possible_answers": options,
                    },
                }
                answerlist.append(answer)
    output = open("questions.json", "w")
    output.write(json.dumps(answerlist))
    output.close()
