import json

from faker import Faker

fake = Faker("de_DE")

if __name__ == "__main__":
    userlist = list()
    for index in range(200):
        profile = fake.profile()
        user = {
            "model": "auth.user",
            "fields": {
                "password": "pbkdf2_sha256$216000$sUn4JQjD6jAS$7KFh93D3fN7X040pVCQh26uztTSEnRrtrwf/4uesx8s=",
                "last_login": "2020-12-29T12:14:40.572Z",
                "is_superuser": False,
                "username": profile.get("username"),
                "first_name": fake.first_name(),
                "last_name": fake.last_name(),
                "email": profile.get("mail"),
                "is_staff": False,
                "is_active": True,
                "date_joined": "2020-12-29T11:30:59.497Z",
                "groups": [],
                "user_permissions": [],
            },
        }
        userlist.append(user)
    output = open("users.json", "w")
    output.write(json.dumps(userlist))
    output.close()
