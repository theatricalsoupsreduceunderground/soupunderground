import json
import re

import requests

if __name__ == "__main__":

    questionspage = "https://conversationstartersworld.com/250-conversation-starters/"
    req = requests.get(questionspage)
    if req.ok:
        raw_questions = re.findall(r"<h3>.*\?", req.text)
        questions = [re.sub(r"<h3>\d*\.\s", "", q) for q in raw_questions]
        questionlist = []
        for q in questions:
            question = {
                "model": "core.customprofilequestion",
                "fields": {
                    "profile_question": q,
                    "pub_date": "2021-01-07T15:51:40.404Z",
                },
            }
            questionlist.append(question)
    output = open("profilequestions.json", "w")
    output.write(json.dumps(questionlist))
    output.close()
