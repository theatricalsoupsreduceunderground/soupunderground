from django.urls import path

from . import views

app_name = "core"
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("my_account/", views.my_account_view, name="my_account"),
    path("profile/<slug:username>", views.profile, name="profile"),
    path("potential_matches", views.match_view, name="potential_matches"),
    path("signup/", views.signup_view, name="signup"),
    path("answer_question/<int:question_id>", views.answer_questions, name="answer_question"),
    path("answer_question/", views.answer_random_questions, name="answer_random_question"),
    path("add_question/", views.add_question, name="add_question"),
]
