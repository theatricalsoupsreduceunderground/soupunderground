from django.contrib import admin

from .models import Answer, CustomProfileAnswer, CustomProfileQuestion, Question, UserProfile


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {"fields": ["question_text", "possible_answers"]}),
    ]
    inlines = []
    list_display = ("question_text", "pub_date")
    list_filter = ["pub_date"]
    search_fields = ["question_text"]


class AnswerAdmin(admin.ModelAdmin):
    fieldsets = [
        (
            None,
            {
                "fields": [
                    "user",
                    "question",
                    "answer_self",
                    "answer_other",
                    "importance",
                    "hide_my_answer",
                ]
            },
        ),
    ]
    list_display = (
        "user",
        "hide_my_answer",
        "created_at",
        "changed_at",
    )
    list_filter = ["created_at", "changed_at", "user"]
    search_fields = ["question"]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(CustomProfileQuestion)
admin.site.register(CustomProfileAnswer)
admin.site.register(UserProfile)
