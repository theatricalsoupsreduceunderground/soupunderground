from datetime import datetime
from math import acos, cos, pi, sin, sqrt
from random import randint

from django.contrib.auth.models import User
from django.db import models
from django.db.models.aggregates import Count
from django.utils.translation import ugettext_lazy as _


class QuestionManager(models.Manager):
    def random(self):
        """ will select a random row from the database in an efficient way """
        count = self.aggregate(ids=Count("id"))["ids"]
        random_index = randint(0, count - 1)
        return self.all()[random_index]


class Question(models.Model):
    question_text = models.CharField(max_length=500)
    pub_date = models.DateTimeField("date published", auto_now_add=True)
    possible_answers = models.JSONField(default=list)

    def __str__(self):
        return self.question_text

    objects = QuestionManager()


class Answer(models.Model):
    class Meta:
        unique_together = (
            "user",
            "question",
        )

    class Importance(models.IntegerChoices):
        NOT_IMPORTANT = 0, _("doesn't matter at all")
        SLIGHTLY = 1, _("a little")
        MEDIUM = 50, _("average")
        VERY_IMPORTANT = 250, _("very important")
        MANDATORY = 300, _("mandatory")

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_self = models.CharField(max_length=128, null=True)
    answer_other = models.JSONField(default=list)
    importance = models.IntegerField(choices=Importance.choices, default=50)
    irrelevant = models.BooleanField(default=False)
    hide_my_answer = models.BooleanField(default=False)
    created_at = models.DateTimeField("date answered", auto_now_add=True)
    changed_at = models.DateTimeField("date last updated", auto_now=True)

    def __str__(self):
        return f"{self.user.username}: {self.question.question_text}"


class MatchingCache(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    other_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="other_user")
    forward_score = models.DecimalField(default=0, decimal_places=1, max_digits=5)
    backward_score = models.DecimalField(default=0, decimal_places=1, max_digits=5)
    combined_score = models.DecimalField(default=0, decimal_places=1, max_digits=5)


class Location(models.Model):
    name_of_location = models.CharField(max_length=128, null=False, unique=True)

    longitude = models.FloatField()
    latitude = models.FloatField()

    created_at = models.DateTimeField("date added", auto_now_add=True)
    changed_at = models.DateTimeField("date last updated", auto_now=True)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, null=True, on_delete=models.SET_NULL)
    location_description = models.CharField(max_length=100)
    birthdate = models.DateField(null=True)

    @property
    def age(self):
        return int((datetime.now().date() - self.birthdate).days / 365.25)


class CustomProfileQuestion(models.Model):
    profile_question = models.CharField(max_length=500)
    pub_date = models.DateTimeField("date published", auto_now_add=True)

    def __str__(self):
        return self.profile_question


class CustomProfileAnswer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(CustomProfileQuestion, on_delete=models.CASCADE)
    answer = models.TextField()
    importance = models.IntegerField(default=1)
    created_at = models.DateTimeField("date answered", auto_now_add=True)
    changed_at = models.DateTimeField("date last updated", auto_now=True)

    def __str__(self):
        return f"{self.user.username}: {self.question.profile_question}"


class GeographicCoordinates(object):
    """Geopraphic coordinates represented by latitude (north=positive degrees, south=negative degrees),
    and longitude (west in positive degrees, east in negative degrees)"""

    _earth_radius = (6357 + 6371) / 2  # km

    def __init__(self, latitude: float, longitude: float):
        # calculate coordinates in radians
        to_radians = pi / 180
        self.lat_rad = latitude * to_radians
        self.long_rad = -longitude * to_radians
        self.theta = abs(90 - latitude) * to_radians
        self.phi = -longitude * to_radians

    def __str__(self):
        return f"{str(self.lat_rad)}, {str(self.long_rad)}"

    def direct_distance_in_space(self, other: "GeographicCoordinates") -> float:
        """ calculates the direct distance of two points on this planet, assuming it's a perfect sphere """

        inner = 1 - sin(self.phi) * sin(other.phi) * cos(self.theta - other.theta) - cos(other.theta) * cos(self.theta)
        root = sqrt(2 * inner)
        return self._earth_radius * root

    def great_circle_distance(self, other: "GeographicCoordinates") -> float:
        """ calculates the distance distance along the surface """
        sinus_part = sin(self.lat_rad) * sin(other.lat_rad)
        cosine_part = cos(self.lat_rad) * cos(other.lat_rad) * cos(abs(self.long_rad - other.long_rad))
        return self._earth_radius * acos(sinus_part + cosine_part)


class Ad(models.Model):
    by_user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    changed_at = models.DateTimeField(auto_now=True)

    placed_in_latitude = models.FloatField()
    placed_in_longitude = models.FloatField()

    def get_coordinates(self) -> GeographicCoordinates:
        return GeographicCoordinates(latitude=self.placed_in_latitude, longitude=self.placed_in_longitude)

    def set_coordinates(self, coordinates: GeographicCoordinates):
        self.placed_in_latitude = coordinates.lat_rad
        self.placed_in_longitude = coordinates.long_rad

    visible_distance = models.IntegerField(help_text="Distance in which this ad is visible km")
    visible_time = models.IntegerField(help_text="Time in hours for which this is visible since the last change.")

    self_destruct = models.BooleanField()
    self_destruct_time = models.IntegerField(
        help_text="Self-destruct time in hours until which this ad is picked up py cleaner tasks"
    )
