import collections

import markdown
from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_list_or_404, get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .forms import AddQuestionForm, AnswerForm, DeleteAccountForm, ImportDataForm, RequestDataForm, myUserCreationForm
from .matching import chunk_into_tuple
from .models import Answer, CustomProfileAnswer, MatchingCache, Question, User

# from .matching import update_matches


@login_required
def my_account_view(request):
    def render_with_forms(
        password_form=None, delete_form=None, request_data_form=RequestDataForm(), import_data_form=ImportDataForm()
    ):
        return render(
            request,
            "account.html",
            {
                "user": user,
                "password_form": password_form,
                "delete_form": delete_form,
                "request_data_form": request_data_form,
                "import_data_form": import_data_form,
            },
        )

    user = request.user
    if request.method == "POST":
        # form needs to contain the field password=true if it is about changing the password
        if request.POST.get("password") == "true":
            print("wants to change password")
            password_form = PasswordChangeForm(request.user, request.POST)
            if password_form.is_valid():
                user = password_form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, "Your password was successfully updated!")
                return redirect(reverse("core:my_account"))
            else:
                return render_with_forms(password_form=password_form, delete_form=DeleteAccountForm())
        # form needs to contain the field delete=true if it is about deleting the account
        elif request.POST.get("delete") == "true":
            delete_form = DeleteAccountForm(request.POST, user=request.user)
            if delete_form.is_valid():
                #  do the deed
                try:
                    username = request.user.username
                    request.user.delete()
                    messages.success(request, f"User {username} was deleted.")
                except Exception as e:
                    messages.error(request, f"Error {str(e)}")
                return redirect(reverse("core:my_account"))
            else:
                messages.warning(request, "please confirm and type your correct password")
                return render_with_forms(PasswordChangeForm(request.POST), delete_form=delete_form)
        else:
            messages.error(request, "unknown form request")
            return redirect(reverse("core:my_account"))
    else:
        return render_with_forms(password_form=PasswordChangeForm(request.user), delete_form=DeleteAccountForm())


class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = "index.html"
    context_object_name = "latest_question_list"

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by("-pub_date")


@login_required
def profile(request, username):
    req_user = User.objects.get(username=username)
    user_self = request.user
    custom_answers = CustomProfileAnswer.objects.filter(user=req_user.id).order_by("importance")
    question_answers = Answer.objects.filter(user=req_user.id)
    html_answers = [
        [custom_answer.question, markdown.markdown(custom_answer.answer, safe_mode=True)]
        for custom_answer in custom_answers
    ]
    return render(
        request,
        "profile.html",
        {
            "req_user": req_user,
            "question_answers": question_answers,
            "html_answers": html_answers,
        },
    )


@login_required
def match_view(request):
    list_of_match_tuples = chunk_into_tuple(
        MatchingCache.objects.filter(user=request.user).filter(forward_score__gte=0).order_by("-forward_score")
    )
    return render(request, "potential_matches.html", {"list_of_match_tuples": list_of_match_tuples})


@login_required
def profile_other(request, username):
    user_id = User.objects.get(username=username)
    answer_list = get_list_or_404(Answer.objects.filter(user=user_id.pk))
    score = get_object_or_404(MatchingCache.objects.filter(user=request.user.pk, other_user=user_id.pk))
    username = username
    return render(request, "profile_other.html", {"answer_list": answer_list, "score": score, "username": username})


def signup_view(request):
    form = myUserCreationForm(request.POST)
    if form.is_valid():
        form.save()
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        user = authenticate(username=username, password=password)
        login(request, user)
        messages.success(request, "Successfully created new account, welcome!")
        return redirect("core:index")
    return render(request, "signup.html", {"form": form})


@login_required
def answer_questions(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    # Fetch a possible already existing answer for this user
    answer = Answer.objects.filter(user=request.user).filter(question=question).first()
    if answer is not None:
        initial = {
            "answer_self": answer.answer_self,
            "answer_other": answer.answer_other,
            "importance": answer.importance,
            "hide_my_answer": answer.hide_my_answer,
        }
    else:
        initial = None

    # evaluate the request
    if request.method == "POST":
        # someone send us a form \o/
        form = AnswerForm(request.POST, question=question, initial=initial)
        if form.is_valid():
            # okay someone had valid answers let's get going
            if answer is None:
                # this seems to be a very fresh answer for this user, create database entry
                answer = Answer.objects.create(
                    question=question,
                    user=request.user,
                )

            if collections.Counter(form.cleaned_data.get("answer_other")) == collections.Counter(
                question.possible_answers
            ):
                answer.irrelevant = True

            # update all the changed fields
            for field in form.changed_data:
                setattr(answer, field, form.cleaned_data.get(field, None))

            answer.save()
            return redirect(reverse("core:index"))
        else:
            return render(request, "answer_question.html", {"question": question, "form": form})
    else:
        # someone wants a form
        form = AnswerForm(question=question, initial=initial)
        return render(request, "answer_question.html", {"question": question, "form": form})


@login_required
def answer_random_questions(request):
    # evaluate the request
    if request.method == "POST":
        question_id = request.POST.get("question_id")
        question = get_object_or_404(Question, pk=question_id)

        # Fetch a possible already existing answer for this user
        answer = Answer.objects.filter(user=request.user).filter(question=question).first()
        if answer is not None:
            initial = {
                "answer_self": answer.answer_self,
                "answer_other": answer.answer_other,
                "importance": answer.importance,
                "hide_my_answer": answer.hide_my_answer,
            }
        else:
            initial = None

        # someone send us a form \o/
        form = AnswerForm(request.POST, question=question, initial=initial)
        if form.is_valid():
            # okay someone had valid answers let's get going
            if answer is None:
                # this seems to be a very fresh answer for this user, create database entry
                answer = Answer.objects.create(
                    question=question,
                    user=request.user,
                )

            if collections.Counter(form.cleaned_data.get("answer_other")) == collections.Counter(
                question.possible_answers
            ):
                answer.irrelevant = True

            # update all the changed fields
            for field in form.changed_data:
                setattr(answer, field, form.cleaned_data.get(field, None))

            answer.save()
            return redirect(reverse("core:answer_random_question"))
        else:
            return render(request, "answer_random_question.html", {"question": question, "form": form})
    else:
        # request.user wants a random question
        user = request.user
        question = Question.objects.random()
        form = AnswerForm(question=question, initial=None)
        return render(request, "answer_random_question.html", {"question": question, "form": form})


def add_question(request):
    user = request.user
    if request.POST:
        form = AddQuestionForm(request_payload=request.POST)
        if form.is_valid():
            answers = list()
            for answer_form in form.formset:
                if answer_form.is_valid():
                    answers.append(answer_form.cleaned_data.get("answer"))
            if len(answers) > 1: # todo: catch non-valid form
                question = form.cleaned_data.get("question_text")
                Question.objects.create(
                    question_text=question,
                    possible_answers=answers,
                )
                # redirect to new question
    else:
        form = AddQuestionForm()

    return render(request, "add_question.html", {"form": form})
