import math
import unittest

import pytest

from core.models import GeographicCoordinates

asserts = unittest.TestCase("__init__")


@pytest.fixture
def berlin_coordinates():
    """52.5200° N, 13.4050° E"""
    return GeographicCoordinates(latitude=52.52, longitude=-13.405)


@pytest.fixture
def north_pole():
    return GeographicCoordinates(latitude=90, longitude=0)


@pytest.fixture
def south_pole():
    return GeographicCoordinates(latitude=-90, longitude=0)


@pytest.fixture
def hamburg_coordinates():
    """53.5511° N, 9.9937° E"""
    return GeographicCoordinates(latitude=53.5511, longitude=-9.9937)


@pytest.fixture
def paris_coordinates():
    """48.8566° N, 2.3522° E"""
    return GeographicCoordinates(latitude=48.8566, longitude=-2.3522)


@pytest.fixture
def nyc_coordinates():
    """40.7128° N, 74.0060° W"""
    return GeographicCoordinates(latitude=40.7128, longitude=74.006)


@pytest.mark.django_db
def test_great_circle_distance(
    berlin_coordinates, hamburg_coordinates, paris_coordinates, nyc_coordinates, north_pole, south_pole
):
    asserts.assertAlmostEqual(
        north_pole.great_circle_distance(south_pole), math.pi * GeographicCoordinates._earth_radius, delta=1e-3
    )
    asserts.assertAlmostEqual(berlin_coordinates.great_circle_distance(hamburg_coordinates), 256, delta=2)
    asserts.assertAlmostEqual(berlin_coordinates.great_circle_distance(paris_coordinates), 878, delta=2)
    asserts.assertAlmostEqual(berlin_coordinates.great_circle_distance(nyc_coordinates), 6387, delta=10)
