import pytest
from django.contrib.auth.models import User
from django.urls import reverse


@pytest.fixture
def hello_user(django_user_model) -> User:
    user = django_user_model.objects.create(username="hello")
    user.set_password("world")
    user.save()
    return user


@pytest.mark.django_db
def test_get_index(hello_user, client):
    client.login(username=hello_user.username, password="world")
    response = client.get(reverse("core:index"))
    assert response.status_code == 200
    assert response.content is not None


@pytest.mark.django_db
def test_get_my_account(hello_user, client):
    client.login(username=hello_user.username, password="world")
    response = client.get(reverse("core:my_account"))
    assert response.status_code == 200
    assert response.content is not None


@pytest.mark.django_db
def test_get_my_profile(hello_user, client):
    client.login(username=hello_user.username, password="world")
    response = client.get(reverse("core:profile", kwargs={"username": hello_user.username}))
    assert response.status_code == 200
    assert response.content is not None


@pytest.mark.django_db
def test_get_potential_matches(hello_user, client):
    client.login(username=hello_user.username, password="world")
    response = client.get(reverse("core:potential_matches"))
    assert response.status_code == 200
    assert response.content is not None


@pytest.mark.django_db
def test_get_signup(hello_user, client):
    client.login(username=hello_user.username, password="world")
    response = client.get(reverse("core:signup"))
    assert response.status_code == 200
    assert response.content is not None
