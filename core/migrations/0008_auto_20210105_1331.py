# Generated by Django 3.1.4 on 2021-01-05 13:31

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20210105_1254'),
    ]

    operations = [
        migrations.AddField(
            model_name='customprofileanswer',
            name='changed_at',
            field=models.DateTimeField(auto_now=True, verbose_name='date last updated'),
        ),
        migrations.AddField(
            model_name='customprofileanswer',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='date answered'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customprofileanswer',
            name='importance',
            field=models.IntegerField(default=1),
        ),
    ]
