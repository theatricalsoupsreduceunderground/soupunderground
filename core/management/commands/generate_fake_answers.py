import random

from django.contrib.auth.models import User
from django.core.management import BaseCommand

from core.models import Answer, Question


class Command(BaseCommand):
    args = ""
    help = "Generates fake answers for users"

    def handle(self, *args, **options):
        answers = 0
        for user in User.objects.all():
            number_of_answers = random.randint(0, 600)
            question_selection = random.sample(list(Question.objects.all()), number_of_answers)
            for question in question_selection:
                answer_self = random.sample(question.possible_answers, 1)[0]
                number_of_accepted_options = random.randint(1, len(question.possible_answers))
                answer_other = random.sample(question.possible_answers, number_of_accepted_options)
                hide_my_answer = random.choices([False, True], [0.98, 0.02])[0]
                importance = random.sample(Answer.Importance.choices, 1)[0]
                Answer.objects.create(
                    user=user,
                    question=question,
                    answer_self=answer_self,
                    answer_other=answer_other,
                    hide_my_answer=hide_my_answer,
                )
                answers += 1
        print(f"Generated {answers} Answers")
