import itertools

from django.contrib.auth.models import User
from django.core.management import BaseCommand

from core.matching import MatchingSimple
from core.models import Answer, MatchingCache, Question


class Command(BaseCommand):
    args = "userid"
    help = "Calculates a forward between two users with username1 and username2"

    def add_arguments(self, parser):
        parser.add_argument("userid", nargs="+", type=int)

    def handle(self, *args, **options):
        user_self = User.objects.get(pk=options.get("userid")[0])

        matching_algo = MatchingSimple()
        for user in User.objects.all().exclude(id=user_self.id):
            forward_score = matching_algo.calculate_forward_match(user_self, user) * 100
            MatchingCache.objects.update_or_create(
                user=user_self,
                other_user=user,
                forward_score=forward_score,
            )
            print(f"{forward_score} match with {user.username}")
