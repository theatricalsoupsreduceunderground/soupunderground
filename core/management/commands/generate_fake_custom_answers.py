import random

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from faker import Faker
from mdgen import MarkdownPostProvider

from core.models import CustomProfileAnswer, CustomProfileQuestion

fake = Faker()
fake.add_provider(MarkdownPostProvider)


class Command(BaseCommand):
    args = ""
    help = "Generates fake custom profile texts for users"

    def handle(self, *args, **options):
        answers = 0
        for user in User.objects.all():
            print(f"User: {user.username}")
            number_of_answers = random.randint(0, 6)
            question_selection = random.sample(list(CustomProfileQuestion.objects.all()), number_of_answers)
            for question in question_selection:
                answer = fake.post()
                importance = random.randint(0, 6)
                CustomProfileAnswer.objects.create(
                    user=user,
                    question=question,
                    answer=answer,
                    importance=importance,
                )
                answers += 1
        print(f"Generated {answers} Answers")
