from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.core.validators import FileExtensionValidator
from django.forms import BaseFormSet, formset_factory

from .models import Answer, Question, User


class myUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "password1", "password2")

    def __init__(self, *args, **kwargs):
        super(myUserCreationForm, self).__init__(*args, **kwargs)

        self.fields["username"].widget.attrs["class"] = "form-control"
        self.fields["password1"].widget.attrs["class"] = "form-control"
        self.fields["password2"].widget.attrs["class"] = "form-control"


class AnswerForm(forms.Form):
    answer_self = forms.ChoiceField(widget=forms.RadioSelect)
    answer_other = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)
    importance = forms.ChoiceField()
    hide_my_answer = forms.BooleanField(required=False)
    question_id = forms.IntegerField(widget=forms.HiddenInput, required=True)

    def __init__(self, *args, question: Question = None, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        self.fields["answer_self"].choices = [(option, option) for option in question.possible_answers]
        self.fields["answer_other"].choices = [(option, option) for option in question.possible_answers]
        self.fields["importance"].choices = Answer.Importance.choices
        self.fields["question_id"].initial = question.id


class DeleteAccountForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput, required=True, help_text="Please type your password!")
    confirmed = forms.BooleanField(
        widget=forms.CheckboxInput, required=True, help_text="Deleting your Account will delete all your data!"
    )
    delete = forms.CharField(widget=forms.HiddenInput, initial="true")

    def __init__(self, *args, user: User = None, **kwargs):
        super(DeleteAccountForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean_password(self):
        if self.user is None:
            raise forms.ValidationError("User not given, cannot validate")

        valid = self.user.check_password(self.cleaned_data["password"])
        if not valid:
            raise forms.ValidationError("Password Incorrect")
        return valid


class RequestDataForm(forms.Form):
    data_request = forms.CharField(widget=forms.HiddenInput, initial="true")


class ImportDataForm(forms.Form):
    data_import = forms.CharField(widget=forms.HiddenInput, initial="true")
    file = forms.FileField(validators=[FileExtensionValidator(allowed_extensions=["json"])])


class PossibleAnswerForm(forms.Form):
    answer = forms.CharField(
        max_length=500, widget=forms.TextInput(attrs={"class": "input", "placeholder": "Possible answer"})
    )


AnswerFormSet = formset_factory(PossibleAnswerForm, extra=2)


class AddQuestionForm(forms.Form):
    def __init__(self, *args, request_payload=None, **kwargs):
        if request_payload is not None:
            super(AddQuestionForm, self).__init__(*args, request_payload, **kwargs)
            self.formset = AnswerFormSet(request_payload)
        else:
            super(AddQuestionForm, self).__init__(*args, **kwargs)

    question_text = forms.CharField(
        widget=forms.TextInput(attrs={"class": "textarea is-primary ", "placeholder": "Your question", "rows": "3"}),
        max_length=500,
        required=True,
    )
    formset = AnswerFormSet()
