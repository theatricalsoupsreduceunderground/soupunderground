import itertools
from abc import ABC, abstractmethod

from django.db.models import QuerySet

from core.models import Answer, Question, User


def retrieve_mutual_questions(user_self: User, user_other: User) -> QuerySet:
    return Question.objects.filter(answer__user=user_other).filter(answer__user=user_self)


def acceptable_forward_answer(answer_self: Answer, answer_other: Answer) -> bool:
    return answer_other.answer_self in answer_self.answer_other


def chunk_into_tuple(iterable):
    it = iter(iterable)
    while True:
        chunk = tuple(itertools.islice(it, 2))
        if not chunk:
            break
        yield chunk


def get_answer_tuplelist(question_queryset, user_self, user_other):
    answer_set = Answer.objects.filter(question__in=question_queryset, user__in=[user_self, user_other])
    if user_self.id < user_other.id:
        answer_set = answer_set.order_by("question_id", "user_id")
    else:
        answer_set = answer_set.order_by("question_id", "-user_id")

    # until here all operations should be executed on the database level
    # the result should be just sorted into tuples with (answer_self, answer_other)
    return [x for x in chunk_into_tuple(answer_set.iterator())]


class MatchingAlgorithm(ABC):
    @abstractmethod
    def calculate_forward_match(self, user_self: User, user_other: User) -> float:
        pass


class MatchingSimple(MatchingAlgorithm):
    def calculate_forward_match(self, user_self: User, user_other: User) -> float:
        full_set: QuerySet = retrieve_mutual_questions(user_self, user_other)
        # mandatory question
        mandatory = full_set.filter(answer__importance=int(Answer.Importance.MANDATORY))
        mandatory_answers = get_answer_tuplelist(mandatory, user_self, user_other)
        if False in [
            acceptable_forward_answer(answer_self, answer_other) for (answer_self, answer_other) in mandatory_answers
        ]:
            return -1
        # non-mandatory questions
        non_mandatory = full_set.difference(mandatory)
        our_relevant_questions = Question.objects.filter(answer__irrelevant=False, answer__user=user_self)
        only_relevant_questions = set(non_mandatory.intersection(our_relevant_questions))
        if len(only_relevant_questions) == 0:
            return 0

        non_mandatory_answers = get_answer_tuplelist(only_relevant_questions, user_self, user_other)
        scorelist = [
            int(acceptable_forward_answer(answer_self, answer_other)) * int(answer_self.importance)
            for (answer_self, answer_other) in non_mandatory_answers
        ]
        average_score = sum(scorelist) / len(scorelist)
        importance_list = [answer_self.importance for (answer_self, _) in non_mandatory_answers]
        average_importance = sum(importance_list) / len(importance_list)
        return average_score / average_importance
