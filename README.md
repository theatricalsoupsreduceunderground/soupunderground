# Soup Underground

Soup underground is a collaborative attempt to design a dating platform suited for queer peeepz.

## Our Ideals, Statements of Intent
this app will always be:

- as inclusive and queer friendly as possible
- no space for racism, transphobia, queerphobia, ableism, fatphobia, misogyny, antisemitism, saneism etc.
- focus on the possibility of privacy
- be open source
- focus on accessibility

this app will never:

- sell any data / use it for anything other than what you signed up for
- be anti-sex-work


## Code Contributions

### Development Setup
Linux or Mac Requirements
- Python 3.8
- Make
- Probably: Postgres libraries (postgresql-libs on arch, libpq-dev on debian?)

### Git Hooks in .githooks

If you want to use the project githooks in .githooks, please execute `git config core.hookspath ./.githooks` in the project root directory.

### Tooling commands
Some of these commands (setup, check, test) are executed every time you commit to the main branch of the repository.
If one fails the commit will *not be deployed* to staging until the pipeline is fixed.

- `make setup`: will remove the virtualenv and install a fresh one with all dependencies listed in `Pipfile.lock`
- `make format`: will use black to autoformat all python code
- `make check`: will run linting checks, security checks and django checks on your local version of the code
- `make test`: will run all pytest tests :-)



### Getting a local version running
This runs on a local SQLite file (db.sqlite3).

- `echo SECRET=SUPERDUPERAPPSECRET >> .env`
- `make setup`
- `./manage.py migrate`
- `./manage.py createsuperuser`
- `./manage.py runserver`

Migration has to be repeated if your model changes generate new migrations.


### How to contribute code

- In order to avoid messy merge commits please add the following lines to your gitconfig
```
[pull]
	rebase = true
```

- Talk to the maintainers to get you added into the group.
- Checkout the main branch, pick up an issue, make some changes (run the tooling) and commit straight to main.

### Merge conflicts.
If people have pushed a commit to main while you were locally committing new changes run a `git pull`.
In most cases this should fast-forward the git history.

If you happen to have a merge conflict, you will have to locally resolve the conflict:
- After executing `git pull` you will end up with an unfinished rebase!
- Manually solve the conflicts in the code.
- Stage all resolved conflict files with `git add [affected_file]`
- Execute `git rebase --continue`
