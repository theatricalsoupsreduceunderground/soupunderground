from django import forms
from django.contrib.auth.forms import AuthenticationForm


class MyLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(MyLoginForm, self).__init__(*args, **kwargs)
        # self.fields['username'].widget.attrs.update()
        # self.fields['password'].widget.attrs.update()

    username = forms.CharField(
        required=True, widget=forms.TextInput(attrs={"class": "input", "type": "text", "placeholder": "Username"})
    )

    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={"class": "input", "type": "password", "placeholder": "Password"}),
    )
