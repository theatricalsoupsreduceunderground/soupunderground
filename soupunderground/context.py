from soupunderground.settings import BASE_URL


def global_context(request):
    return {"base_url": BASE_URL}
